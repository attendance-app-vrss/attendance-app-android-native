package com.example.roshan.attendanceapp

import android.content.Context
import android.util.Log
import android.widget.Toast

object Utility {

    //Developer methods
    //To log messages

    fun logAsDeveloper(type: String,  text: String) {
        if(BuildConfig.IS_DEV) {
            Log.i(type, text)
        }
    }

    fun toastAsDeveloper(context: Context, text: String) {
        if(BuildConfig.IS_DEV) {
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
        }
    }
}