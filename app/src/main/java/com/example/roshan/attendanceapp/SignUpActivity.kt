package com.example.roshan.attendanceapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class SignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
    }

    fun handleStudentButton(view: View) {
        val intentToLoginAsStudent = Intent(this, LoginActivity::class.java)
        intentToLoginAsStudent.putExtra("accountType", Constants.LOGGED_IN_AS_STUDENT)
        startActivity(intentToLoginAsStudent)
    }

    fun handleTeacherButton(view: View) {
        val intentToLoginAsTeacher = Intent(this, LoginActivity::class.java)
        intentToLoginAsTeacher.putExtra("accountType", Constants.LOGGED_IN_AS_TEACHER)
        startActivity(intentToLoginAsTeacher)
    }

    fun handleAdminButton(view: View) {
        val intentToLoginAsAdmin = Intent(this, LoginActivity::class.java)
        intentToLoginAsAdmin.putExtra("accountType", Constants.LOGGED_IN_AS_ADMIN)
        startActivity(intentToLoginAsAdmin)
    }


}
