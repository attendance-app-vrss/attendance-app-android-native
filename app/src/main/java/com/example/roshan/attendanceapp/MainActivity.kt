package com.example.roshan.attendanceapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sharedPref = getSharedPreferences("session", Context.MODE_PRIVATE)
        val isLogged = sharedPref.getInt("loggedIn", Constants.NOT_LOGGED_IN)
        if(isLogged != Constants.NOT_LOGGED_IN) {
            val accountType = sharedPref.getInt("accountType", Constants.LOGGED_IN_AS_STUDENT)
            val accountId = sharedPref.getString("accountId", Constants.ID_NOT_AVAILABLE)
            val accountPassword = sharedPref.getString("accountPassword", Constants.PASSWORD_NOT_AVAILABLE)
            when (accountType) {
                Constants.LOGGED_IN_AS_STUDENT ->  {
                    //Log in to student accountId and accountPassword
                    //Intent to student activity
                    return
                }
                Constants.LOGGED_IN_AS_TEACHER -> {
                    //Log in to teacher accountId and accountPassword
                    //Intent to teacher activity
                    return
                }
                Constants.LOGGED_IN_AS_ADMIN -> {
                    //Log in to admin with accountId and accountPassword
                    //Intent to admin activity
                    return
                }
            }
        } else {
            //Handling not logged in case.
            val intentToSignUp = Intent(this, SignUpActivity::class.java)
            startActivity(intentToSignUp)
            Utility.toastAsDeveloper(this, "No logged in info found.")
            finish()
        }
    }
}
