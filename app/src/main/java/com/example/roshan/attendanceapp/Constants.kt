package com.example.roshan.attendanceapp

object Constants {

    public val NOT_LOGGED_IN = 0
    public val LOGGED_IN_AS_STUDENT = 1
    public val LOGGED_IN_AS_TEACHER = 2
    public val LOGGED_IN_AS_ADMIN = 3
    public val ID_NOT_AVAILABLE = null
    public val PASSWORD_NOT_AVAILABLE = null
}