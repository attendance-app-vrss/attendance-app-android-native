package com.example.roshan.attendanceapp

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val accountType = intent.getIntExtra("accountType", Constants.NOT_LOGGED_IN)

        when(accountType){
            Constants.LOGGED_IN_AS_STUDENT -> {
                //Handle student login parts
                accountTypeTextView.text = "Log in to your Student Account"
            }
            Constants.LOGGED_IN_AS_TEACHER -> {
                //Handle teacher login parts
                accountTypeTextView.text = "Log in to your Teacher Account"

            }
            Constants.LOGGED_IN_AS_ADMIN -> {
                //Handle admin login parts
                accountTypeTextView.text = "Log in to your Administrator Account"

            }
            Constants.NOT_LOGGED_IN -> {
                Utility.toastAsDeveloper(this, "Weird error, should not happen.")
            }
        }
    }

    fun back(view: View) {
        onBackPressed()
    }
}
